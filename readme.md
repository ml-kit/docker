# Local deploy

Clone repository, update all submodules:

```bash
$ git clone --recurse-submodules -j8 git@gitlab.com:ml-kit/docker.git ml-kit
$ cd ml-kit
$ git submodule foreach --recursive "git checkout dev || git checkout master || true"
$ git submodule foreach --recursive "git pull"
```


Create .env file from scratch or use predefined sample:


```bash
$ cp .env.sample .env
```


[Optional] Create local docker-compose.override.yml configuration based on provided samples or just make symlink and start compose:


```bash
$ ln -s local.yml docker-compose.override.yml
$ docker-compose up
```
