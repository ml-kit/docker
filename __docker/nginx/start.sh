#!/bin/sh

# append optional contents of HTPASSWD variable to auth file
echo $HTPASSWD >> /etc/nginx/.htpasswd

# run nginx in foreground
nginx -g "daemon off;"
